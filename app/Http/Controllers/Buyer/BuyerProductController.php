<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        //find and return all product of a specific buyer
        $products = $buyer->transactions()->with('product')
        ->get()
        ->pluck('product');

        return $this->showAll($products);
    }
}
