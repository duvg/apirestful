<?php

namespace App\Http\Controllers\Seller;

use App\User;
use App\Seller;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        //find and return all product of seller
        $products = $seller->products;

        return $this->showAll($products);
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $seller)
    {
        //save product a specific seller

        //rules to validate data in request
        $rules = [
            'name'        => 'required',
            'description' => 'required',
            'quantity'    => 'required|integer|min:1',
            'image'       => 'required|image',
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        $data['status'] = Product::PRODUCTO_NO_DISPONIBLE;
        $data['image'] = '1.png';
        $data['seller_id'] = $seller->id;

        $product = Product::create($data);

        return $this->showOne($product, 201);


    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seller $seller, Product $product)
    {
        //update a specific product of a selelr
        //rules tovalidate request
        $rules = [
            'name' => 'min:2',
            'quantity' => 'integer|min:1',
            'status' => 'in: ' . Product::PRODUCTO_DISPONIBLE . ',' . Product::PRODUCTO_NO_DISPONIBLE,
            'image' => 'image',
        ];

        $this->validate($request, $rules);

        //verification id seller of product
        $this->verifySeller($seller, $product);

        $product->fill($request->only(
            'name',
            'description',
            'quantity'
        ));

        if ($request->has('status')) {
            $product->status = $request->status;

            //validate cahnge status if product has categories
            if ($product->estaDiponible() && $product->categories()->count() == 0) {
                return $this->errorResponse('Un producto activo debe tener almenos una categoria', 409);
            }
        }


        if ($product->isClean()) {
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }

        //save changes
        $product->save();

        return $this->showOne($product);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller, Product $product)
    {
        //verification id seller of product
        $this->verifySeller($seller, $product);

        $product->delete();

        return $this->showOne($product);
    }

    //vefiry seller is the owner of the product
    public function verifySeller(Seller $seller, Product $product)
    {
        if ($seller->id  != $product->seller_id)
        {
            throw new HttpException(422, 'No eres el vendedor de este producto');
        }
    }

}
